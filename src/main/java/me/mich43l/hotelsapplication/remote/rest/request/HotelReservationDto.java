package me.mich43l.hotelsapplication.remote.rest.request;

public class HotelReservationDto {
    private int id;
    private int roomId;
    private int days;

    public HotelReservationDto() {
    }

    public HotelReservationDto(int id, int roomId, int days) {
        this.id = id;
        this.roomId = roomId;
        this.days = days;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getRoomId() {
        return roomId;
    }

    public void setRoomId(int roomId) {
        this.roomId = roomId;
    }

    public int getDays() {
        return days;
    }

    public void setDays(int days) {
        this.days = days;
    }
}