package me.mich43l.hotelsapplication.remote.rest.response;

import java.util.List;

public class HotelListDto {
    private List<HotelDto> hotels;

    public HotelListDto() {
    }

    public HotelListDto(List<HotelDto> hotels) {
        this.hotels = hotels;
    }

    public List<HotelDto> getHotels() {
        return hotels;
    }

    public void setHotels(List<HotelDto> hotels) {
        this.hotels = hotels;
    }
}
