package me.mich43l.hotelsapplication.remote.rest.response;

import me.mich43l.hotelsapplication.domain.model.StatusType;
import me.mich43l.hotelsapplication.remote.rest.request.HotelReservationDto;
import me.mich43l.hotelsapplication.remote.rest.request.PersonReservationDto;

import java.util.List;

public class ReservationDto {
    private int id;
    private StatusType status;
    private List<HotelReservationDto> hotels;
    private PersonReservationDto person;

    public ReservationDto() {
    }

    public ReservationDto(int id, StatusType status, List<HotelReservationDto> hotels, PersonReservationDto person) {
        this.id = id;
        this.status = status;
        this.hotels = hotels;
        this.person = person;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public StatusType getStatus() {
        return status;
    }

    public void setStatus(StatusType status) {
        this.status = status;
    }

    public List<HotelReservationDto> getHotels() {
        return hotels;
    }

    public void setHotels(List<HotelReservationDto> hotels) {
        this.hotels = hotels;
    }

    public PersonReservationDto getPerson() {
        return person;
    }

    public void setPerson(PersonReservationDto person) {
        this.person = person;
    }
}
